package come.scujcc.welcomeactivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AccountflagActivity {
    EditText txtFlag;//创建EditText组件对象
    Button btnflagSaveButton;//创建Button组件对象
    Button btnflagCancelButton;
    protected void onCreate(){
        txtFlag=(EditText) txtFlag.findViewById(R.id.txtFlag);//获取标签文本框
        btnflagSaveButton=(Button) btnflagSaveButton.findViewById(R.id.btnflagSave);//获取”保存“按钮
        btnflagCancelButton=(Button) btnflagCancelButton.findViewById(R.id.btnflagCancel);//获取”取消“按钮
        btnflagSaveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                //TODO Auto-generated method stub
                String strFlag=txtFlag.getText().toString();     //获取标签文本框的值
                if(!strFlag.isEmpty()){                          //判断获取的值不为空
                    FlagDAO flagDAO= new FlagDAO(AccountflagActivity.this) {
                        @Override
                        public Object getMaxld() {
                            return null;
                        }

                        @Override
                        public void add(Tb_flag tb_flag) {

                        }
                    };
                    Tb_flag tb_flag=new Tb_flag(flagDAO.getMaxld()+1,strFlag);//创建Tb_flag对象
                    flagDAO.add(tb_flag);               //添加便签信息
                    //弹出信息提示
                    Toast.makeText(AccountflagActivity.this,"（新建便签）数据添加成功！",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(AccountflagActivity.this,"请输入便签！",Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnflagCancelButton.setOnClickListener(new View.OnClickListener() {     //为”取消“按钮设置监听事件
            @Override
            public void onClick(View arg0) {
                //TODO Auto-generated method stub
                txtFlag.setText("");                            //“清空便签文本框”
            }
        });
    }

}
