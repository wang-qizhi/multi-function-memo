package cn.edu.scujcc;

import java.util.List;

public class User {
	
	private String id;//账号
	private String username;//用户名
	private String email;//邮箱
	private int gender;//性别
	private int portrait;//头像
	private List<Note> recovery;//回收站
	private List<Note> MyNotes;//我的备忘录
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getPortrait() {
		return portrait;
	}
	public void setPortrait(int portrait) {
		this.portrait = portrait;
	}
	public List<Note> getRecovery() {
		return recovery;
	}
	public void setRecovery(List<Note> recovery) {
		this.recovery = recovery;
	}
	public List<Note> getMyNotes() {
		return MyNotes;
	}
	public void setMyNotes(List<Note> myNotes) {
		MyNotes = myNotes;
	}
	
	
}
