package cn.edu.scujcc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Userservice {
	@Autowired
	private UserRepository repo;
	
	/**
	 * 找到所有用户
	 * @return
	 */
	public List<User> getAllUser() {
		List<User> result = null;
		result =repo.findAll();
		return result;
	}
	public User Mynotes(String id, Note n) {
		//1.根据id从数据库中读取(已有)便签
		User u;
		u.getMyNotes() = new ArrayList();
		u.getMyNotes().add(n);
		Mynotes result = null;
		result = getMynotes(id);
		//2.让新的便签更新
		result = repo.save(u);
		return result;
	}
}
