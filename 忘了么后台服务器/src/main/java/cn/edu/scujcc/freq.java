package cn.edu.scujcc;

public class freq {
	private String type;//时间类型(年，月，日，天)
	private int value;//确定时间数值
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
//确定时间:时间类型+时间数值
}
