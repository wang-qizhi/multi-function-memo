package come.scujcc.welcomeactivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
public class MoreActivity extends AppCompatActivity {
    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_more);

        @SuppressLint("WrongViewCast") Button btn = findViewById(R.id.btn_userset);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(MoreActivity.this, UsersetActivity.class);
                MoreActivity.this.startActivity(it);
            }
        });
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_more);

        @SuppressLint("WrongViewCast") Button btn2 = findViewById(R.id.btn_super);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(MoreActivity.this, UsersuperActivity.class);
                MoreActivity.this.startActivity(it);
            }
        });

    }
}
