package com.scujjc.mymemo.ui.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.scujjc.mymemo.R;
import com.scujjc.mymemo.databinding.FragmentNotificationsBinding;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private FragmentNotificationsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                new ViewModelProvider(this).get(NotificationsViewModel.class);

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Button btn =  root.findViewById(R.id.btn12);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent it = new Intent();
                it.setClass(getActivity(), DeleteActivity.class);
                getActivity().startActivity(it);
            }
        });

        ImageView testImage=binding.cover;
        String url=
                "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201508%2F20%2F20150820124615_vBLy8.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625880937&t=001fc10628323ad23ac79886349d75eb";

        RequestOptions ro=RequestOptions.circleCropTransform().override(280,280);
        Glide.with(this)
                .load(url)
                .apply(ro)
                .into(testImage);

        Button btn1 = binding.btn3;
        btn1.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
                Intent it = new Intent();
                it.setClass(getActivity(), RingActivity.class);
                startActivity(it);
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}