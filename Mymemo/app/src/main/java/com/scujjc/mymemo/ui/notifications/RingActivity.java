package com.scujjc.mymemo.ui.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.scujjc.mymemo.R;

public class RingActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ring);
        Button btn = findViewById(R.id.btn11);
        btn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
                Intent it = new Intent();
                it.setClass(RingActivity.this,NotificationsFragment.class);
                RingActivity.this.startActivity(it);
            }
        });


    }
}