package com.scujjc.mymemo.ui.set;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.scujjc.mymemo.MainActivity;
import com.scujjc.mymemo.R;
import com.scujjc.mymemo.RegisterActivity;
import com.scujjc.mymemo.databinding.FragmentSetBinding;
public class SetFragment extends Fragment {
    private SetViewModel setViewModel;
    private FragmentSetBinding binding;
    private Object SetViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        SetViewModel =
                new ViewModelProvider(this).get(SetViewModel.class);

        binding = FragmentSetBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Button btn = root.findViewById(R.id.btn_register);
        Button btn1=root.findViewById(R.id.btnLogin);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent it = new Intent();
                it.setClass(getActivity(), RegisterActivity.class);
                getActivity().startActivity(it);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
                Intent it = new Intent();
                it.setClass(getActivity(), MainActivity.class);
                startActivity(it);
            }

        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
